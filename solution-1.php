<?php

function solve($word)
{
	$vowels = ['a', 'e', 'i', 'o', 'u'];
	$count = 0;
	$arr = str_split($word);
	foreach ($arr as $char) {
		in_array($char, $vowels) ? $count++ : $count;
	}

	return pow(2,$count);
}

echo solve(readline("Enter you word: "));