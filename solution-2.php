<?php

class Student
{
	private $id, $name, $field, $entering_year;
	public $students = array();

	public function __construct($id, $name, $field, $entering_year)
	{
		$this->id = $id;
		$this->name = $name;
		$this->field = $field;
		$this->entering_year = $entering_year;
	}
}